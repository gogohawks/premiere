const https = require('https');
const range = "sales!A1:E6";
const spreadsheetId = "11BCnspCt2Mut3nhc4WMY6CYTd0zF9C3eCzsk1AEpKLM";
const apiKey = "APIKEY";
const URL = "https://sheets.googleapis.com/v4/spreadsheets/" + spreadsheetId + "/values/" + range + "?key=" + apiKey;

https.get(URL, function(res) {
    var body = '';
    res.setEncoding('utf8');
    res.on('data', function(chunk) {
        body += chunk;
    });
    res.on('data', function(chunk) {
        res = JSON.parse(body).values;
        //console.log(res);
        for (var i = 0; i < res.length; i++) {
            var data = res[i];
            console.log("'%s','%s','%s','%s','%s'", data[0], data[1], data[2], data[3], data[4]);
        }
    })
}).on('error', function(e) {
    console.log(e.message);
});
