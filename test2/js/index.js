$(function() {

    var leftCnt = 0;
    var rightCnt = 0;
    $(window).keydown(function(e) {

        switch (e.which) {
            case 39: // Key[→]
            if($("#rect").offset().left + 80 > $("#content").width()) return;
            leftCnt = 0;
                $("#rect").css("background-image", "url(./img/mario_right.png)")
                    .css("background-position", 80 * rightCnt + "px");
                if (rightCnt < 3) {
                    rightCnt++;
                } else {
                    rightCnt = 0;
                }

                if ($("#rect").is(":animated")) return;

                $('#rect').animate({
                    left: '+=10px'
                }, 0);
                break;

            case 37: // Key[←]
            if($("#rect").offset().left < 0) return;
            rightCnt = 0;
                $("#rect").css("background-image", "url(./img/mario_left.png)")
                    .css("background-position", 80 * leftCnt + "px");
                if (leftCnt < 3) {
                    leftCnt++;
                } else {
                    leftCnt = 0;
                }

                if ($("#rect").is(":animated")) return;

                $('#rect').animate({
                    left: '-=10px'
                }, 0);
                break;

            case 38: // Key[↑]
                if ($("#rect").is(":animated")) {
                    return false;
                }
                $('#rect').animate({
                    top: '-=10px'
                }, 0);
                break;

            case 40: // Key[↓]
                if ($("#rect").is(":animated")) {
                    return false;
                }
                $('#rect').animate({
                    top: '+=10px'
                }, 0);
                break;
        }
    });
});
